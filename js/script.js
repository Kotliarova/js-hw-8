// Опишіть своїми словами що таке Document Object Model (DOM)
// DOM це те що формує браузер на основі HTML коду. Браузер створює DOM для легкої маніпуляціії веб-сторінки за допоіогою JS. Наприклад знайти елемент та його змінити або просто додати новий елемент тощо

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// за допомогою innerHTML ми можемо вставляти елемент саме як html, тобто з тегами та всім іншим. innerText дозволяє задавати або отримувати текстовий вміст елемента та його нащадків, тобто не може містити HTML теги;

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//document.getElementById() 
//document.getElementsByName()
//document.getElementsByTagName() 
//document.getElementsByClassName()
// за допомогою 2 нижче можемо отримати все що нам потрібно (ці способи для мене краще)
//document.querySelector() - повертає HTML колекцію (не можемо працювати як з масивом треба робити Array.from)
//document.querySelectorAll() - повертає NodeList (можемо застосовувати метод forEach як для массиву)


// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');
for (const p of paragraphs) {
    p.style.backgroundColor = '#ff0000'
}

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionalList = document.getElementById('optionsList');
console.log(optionalList);
console.log(optionalList.parentElement);
console.log(optionalList.childNodes);

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
document.querySelector('#testParagraph').textContent = 'This is a paragraph';

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const childrenOfMainHeader = document.querySelector('.main-header').children;
console.log(childrenOfMainHeader);
for (const elem of childrenOfMainHeader) {
    elem.classList.add('nav-item');
};

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(element => {
    element.classList.remove('section-title');
})
